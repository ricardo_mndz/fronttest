import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from 'src/service/movies.service';
import { HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Movies } from '../models/movies';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private moviesService: MoviesService,
    private router: Router,
    private httpClient: HttpClient
  ) { }

  idMovie!: number;
  movietitle!: string;
  year!: number;
  duration!: number;

  upMovieTitle! : string;
  upYear! : number;
  upDuration! :number

  deleteId! : number;

  movieId! : number;
  
  ngOnInit(): void {
   console.log('Hola, prueba Nabenik');
  }
  title = 'Front-Test';

  onCreate(){
    this.moviesService.save(new Movies(this.movietitle, this.year, this.duration)).subscribe(
      data => {
        console.log('Movie creada con title: ' + this.movietitle );
      }
    );
  }

  onUpdate(){
    this.moviesService.update(this.idMovie, new Movies(this.upMovieTitle, this.upYear, this.upDuration)).subscribe(
      data => {
        console.log('Movie actualizada con title: ' + this.upMovieTitle );
      }
    );
  }

  onDelete(){
    this.moviesService.delete(this.deleteId).subscribe(
      data => {
        console.log('Movie eliminada con id: ' + this.deleteId );
      }
    );
  }

  onWatch(){
    this.moviesService.verPelicula(this.movieId).subscribe(
      data => {
        console.log('Ver movie con id: ' + this.movieId );
      }
    );
  }

  onSee(){
    this.moviesService.lista().subscribe(
      data => {
        console.log('Mis peliculas' );
      }
    );
  }
}
