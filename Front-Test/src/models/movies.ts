export class Movies{
    id?:number;
    title: string;
    year: number;
    duration: number;

    constructor(title:string, year: number, duration: number){
        this.title = title;
        this.year= year;
        this.duration= duration;
    }

}