import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movies } from '../models/movies';

@Injectable({
    providedIn: 'root'
  })
  export class MoviesService {

    constructor(private http: HttpClient) { }
  
    Url = 'http://localhost:8080/back-test/rest/movies';
  
  
    //CODIGO PARA CRUD DE MOVIES
    public lista(): Observable<Movies[]> {
      return this.http.get<Movies[]>(this.Url+"/title=''");
    }
  
    public verPelicula(id: number): Observable<Movies> {
      return this.http.get<Movies>(this.Url + `/${id}`);
    }
  
    public save(movie: Movies): Observable<any> {
      return this.http.post<any>(this.Url, movie);
    }
  
    public update(id: number, movie: Movies): Observable<any> {
      return this.http.put<any>(this.Url + `/${id}`, movie);
    }
  
    public delete(id: number): Observable<any> {
      return this.http.delete<any>(this.Url + `/${id}`);
    }
  
  }